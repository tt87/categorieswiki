﻿$(function() {
    var categoriesSelect = $("#categories");
    var actionsSelect = $("#actions");
    var optionsSelect = $("#options");
    var positionsSelect = $("#position");
    var fieldsDiv = $("#fields");
    var button = $("#btnGenerate");
    var resultArea = $("#result");
    var tokens = [];
    var renderedTokens;
    var selectedCategory;
    var tokensRegexp = /<\w+>/g;


    loadCategories();

    categoriesSelect.change(update);
    actionsSelect.change(generateRequest);
    optionsSelect.change(generateRequest);
    positionsSelect.change(generateRequest);
    button.click(generateRequest);
    update();

    function loadCategories() {
        var catNames = categories.map(function(x) { return x.Name }).sort();
        $(catNames).each(function (i, data) {
            categoriesSelect.append($("<option></option>").val(data).html(data));
        });

        categoriesSelect.val("Незаконная торговля алкоголем");
    }


    function update() {
        selectedCategory = getCatByName(categoriesSelect.val());
        extractTokens();
        fillSelectFromJson(actionsSelect, "actions", "Actions");
        fillSelectFromJson(optionsSelect, "options", "Options");
        fillSelect(positionsSelect, "position", defaults.position);
        generateFieldInputs();
        generateRequest();
    }

    function getCatByName(catName) {
        return (categories).filter(function(c) { return c.Name === catName })[0];
    }

    function generateFieldInputs() {
        fieldsDiv.html("");

        $(tokens).each(function(i, x) {
            if (renderedTokens[x.toLowerCase()]) return;
            fieldsDiv.append("<p>" + x + "</p>");
            fieldsDiv.append("<input type='text' id='" + x + "'></input>");
            if (defaults[x]) {
                $("#" + x).val(defaults[x]);
            }
            $("#" + x).bind("change paste keyup", generateRequest);
        });
    }

    function extractTokens() {
        tokens = [];
        renderedTokens = { photo: true };
        var match;

        while ((match = tokensRegexp.exec(selectedCategory.RawText)) !== null) {
            if (match.index === tokensRegexp.lastIndex) {
                tokensRegexp.lastIndex++;
            }
            
            tokens.push(match[0].substr(1, match[0].length - 2));
        }
    }

    function fillSelectFromJson(select, tokenName, propertyName) {
        var values = selectedCategory[propertyName];
        fillSelect(select, tokenName, values);
    }

    function fillSelect(select, tokenName, values) {
        select.html("");
        if (values && values.length && tokenExists(tokenName)) {
            renderedTokens[tokenName] = true;
            select.show();
            $(values).each(function(i, x) {
                select.append($("<option></option>").val(x.Text || x).html(x.Name || x));
            });
        } else {
            select.hide();
        }
    }

    function tokenExists(tokenName) {
        return (tokens).filter(function(x) { return x === tokenName }).length > 0;
    }

    function generateRequest() {
        var rawText = selectedCategory.RawText;
        $(tokens).each(function(i, x) {
            var field = $("#" + x);
            var replacement = (field.val()) ? field.val() : "";

            if ($.isArray(replacement)) {
                replacement = replacement.map(function(r) {
                    var trimmed = r.trim();
                    return trimmed;
                });

                replacement = replacement.join("\r\n");
            }

            rawText = rawText.replace("<" + x + ">", "<strong>" + replacement + "</strong>");
        });
        resultArea.html(rawText);
    }

});

